use indicatif::{ProgressBar, ProgressStyle};

/// Gets defaults progress bar style.
pub fn get_progress_bar_style() -> ProgressStyle {
  ProgressStyle::default_bar()
    .template("{msg} {bar:30.cyan/blue} [{eta} remaining]")
    .progress_chars("##-")
}

/// Gets progress bar style with given message and length.
///
/// If `verbose` is `false`, it emits no output.
pub fn get_progress_bar(msg: &'static str, size: u64, verbose: bool) -> ProgressBar {
  if verbose {
    let bar = ProgressBar::new(size).with_style(get_progress_bar_style());
    bar.set_message(msg);
    bar
  } else { ProgressBar::hidden() }
}
