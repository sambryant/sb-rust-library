//! Tiny library used to plot data through graphical primitives.
//!
//! The main object it provides is [`Plot`](struct.Plot.html) which enables
//! plotting data using basic primitives like circles, line segments, and
//! oriented points. You can plot data using any of the shapes provided in
//! [`shapes`](plotter.shapes) through the corresponding draw methods.
//! # Examples
//!
//! Basic sine wave without axis labels
//!
//! ```
//! let mut p = Plot::new(300, 200)
//!   .with_canvas_color(GREY)
//!   .with_bg_color(BLUE)
//!   .with_plotting_range(0.0..(2.0 * PI), -1.1..1.1)
//!   .with_drawing_bounds(0.05..0.95, 0.1..0.95);
//!
//! let x_values: Vec<f64> = (0..1000).map(|i| (i as f64) / 1000.0 * 2.0 * PI).collect();
//! let points: Vec<Point> = x_values.into_iter().map(|x| {
//!   (x, x.sin()).into()
//! }).collect();
//!
//! let circle = Circle::new(RED, 1);
//! p.draw_circles(&circle, &points);
//! p.save("sine-wave.bmp").unwrap();
//! ```

mod color;
mod plot_frame;
mod plot;
mod shapes;

pub use color::*;
pub use plot::Plot;
pub use shapes::{Circle, Orientation};
