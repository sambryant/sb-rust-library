use image::Rgb;

/// A 32-bit color used for plotting.
pub type Color = Rgb<u8>;

/// A nice looking red.
pub const RED: Color = Rgb([243, 97, 97]);
/// A nice looking orange.
pub const ORANGE: Color = Rgb([247, 184, 85]);
/// A nice looking yellow.
pub const YELLOW: Color = Rgb([243, 243, 97]);
/// A nice looking green.
pub const GREEN: Color = Rgb([150, 217, 77]);
/// A nice looking blue.
pub const BLUE: Color = Rgb([129, 196, 238]);
/// A nice looking purple.
pub const PURPLE: Color = Rgb([151, 93, 201]);
/// A nice looking pinkish.
pub const PINK: Color = Rgb([221, 95, 114]);
/// A nice looking white.
pub const WHITE: Color = Rgb([255, 255, 255]);
/// A nice looking black.
pub const BLACK: Color = Rgb([0, 0, 0]);
/// A nice looking mauve.
pub const MAUVE: Color = Rgb([199, 171, 175]);
/// A nice looking greyish.
pub const GREY: Color = Rgb([226, 215, 216]);
