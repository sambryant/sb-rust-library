#![warn(missing_docs)]
//! Basic library providing common functionality I reuse in many of my projects.
//!
//! I plan on adding a lot more things as I go, but for now, the only thing
//! really worth mentioning is the mini plotting library `plotter` which can be
//! used to quickly plot primitives (lines and circles) on a canvas.

pub mod plotter;

/// Wrapper around [indicatif](https://github.com/mitsuhiko/indicatif)'s `ProgressBar` with my preferred style.
pub mod bar;

/// Library that performs very common linear algebra calculations.
pub mod math;

/// Update buffer struct that allows you to update data while preserving old values.
pub mod update_buffer;
