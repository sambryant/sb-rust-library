use std::f64::consts::PI;
use sb_rust_library::{
  plotter::{
    BLUE,
    GREY,
    Plot,
    RED,
  },
};

pub fn main() {
  let mut p = Plot::new(800, 800)
    .with_canvas_color(GREY)
    .with_bg_color(BLUE)
    .with_plotting_range(-1.0..1.0, -1.0..1.0)
    .with_drawing_bounds(0.05..0.95, 0.1..0.95);

  let pts: Vec<(f64, f64)> = (0..6)
    .map(|n| {
      let angle = 2.0 * PI * (n as f64) / 5.0;
      let x = 0.9 * angle.cos();
      let y = 0.9 * angle.sin();
      (x, y)
    })
    .collect();
  p.draw_line(pts[0], pts[1], RED);
  p.draw_line(pts[1], pts[2], RED);
  p.draw_line(pts[2], pts[3], RED);
  p.draw_line(pts[3], pts[4], RED);
  p.draw_line(pts[4], pts[5], RED);
  p.save("line_segments.bmp").unwrap();
}