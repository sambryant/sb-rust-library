use sb_rust_library::{
  math::Point,
  plotter::{
    BLUE,
    Circle,
    GREY,
    Plot,
    RED,
  },
};

use core::f64::consts::PI;

pub fn main() {
  let mut p = Plot::new(300, 200)
    .with_canvas_color(GREY)
    .with_bg_color(BLUE)
    .with_plotting_range(0.0..(2.0 * PI), -1.1..1.1)
    .with_drawing_bounds(0.05..0.95, 0.1..0.95);

  let x_values: Vec<f64> = (0..1000).map(|i| (i as f64) / 1000.0 * 2.0 * PI).collect();
  let points: Vec<Point> = x_values.into_iter().map(|x| {
    (x, x.sin()).into()
  }).collect();

  let circle = Circle::new(RED, 1);
  p.draw_circles(&circle, &points);
  p.save("sine_wave.bmp").unwrap();
}